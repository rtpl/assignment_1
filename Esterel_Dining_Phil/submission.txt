Group Members:
Füß, Dominik: 03614772
Hofbauer, Markus: 03623454
Meyer, Kevin: 03623271


Aufgabe 1. 1 d)

Trace shows, that every philosophers will eat eventually (no starvation) while there is no deadlock (never all philosophers are waiting at the same time).

The trace is saved in the file "trace.esi"

-------------------------------------------
Aufabe 1.1 2.5

The module check is saved in the file "Esterel_Dining_Phil.blif"

Aufgabe 1.1 2.5 a)

Xeve tracks the P<1,..,5>OK signals which should be always present. --> always emitted.

Aufgabe 1.1 2.5 b)

Deadlock signal is never present.

Aufgabe 1.1 2.5 c)

FORK<1,..,5>_DOUBLE signals are never present.

-------------------------------------------
Aufabe 1.1 2.6 (Bonus)

For the new inputs we created a new bilf file, saved in "Esterel_Dining_Phil_Bonus.blif"
